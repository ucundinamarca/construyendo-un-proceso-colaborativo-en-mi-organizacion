var ST = "#Stage_";
var S = "Stage_";
ivo.info({
    title: "Construyendo un proceso colaborativo en mi organización",
    autor: "Edilson Laverde Molina",
    date: "01/02/2021",
    email: "edilsonlaverde_182@hotmail.com",
    icon: 'https://www.ucundinamarca.edu.co/images/iconos/favicon_verde_1.png',
    facebook: "https://www.facebook.com/edilson.laverde.molina"
});
var audios=[
    {url:"sonidos/click.mp3",                       name:"click" },
    {url:"sonidos/RED_Situacion_1.mp3",             name:"caso1" },
    {url:"sonidos/RED_Situacion_2.mp3",             name:"caso2" },
    {url:"sonidos/Relaciones_Confianza_Situa1.mp3", name:"caso12"},
    {url:"sonidos/Relaciones_Confianza_Situa2.mp3", name:"caso22"},
    {url:"sonidos/fail.mp3",                        name:"fail"},
    {url:"sonidos/good.mp3",                        name:"good"},
    {url:"sonidos/regular.mp3",                     name:"regular"}
];
var puntos1=0;
var puntos11=0;
var puntos2=0;
var puntos22=0;
var vistos1=[];
var vistos11=[];
var vistos2=[];
var vistos22=[];
var paquete1=[
    {
        q:"1. Una vez usted ha decidido realizar el proyecto, en cuanto a disposición, usted…",
        r1:"Realiza una investigación sobre el tema central de interés para compartir con sus compañeros.",
        r2:"Identifica cuáles son aquellos compañeros con los que se siente con mayor confianza y los convoca a un encuentro previo.",
        r3:"Escribe una propuesta de proyecto con objetivos definidos para exponer al grupo de compañeros.",
        bueno:"r2",
        malo:"r3",
        regular:"r1"
    },
    {
        q:"2. Su organización considera el trabajo colaborativo como un punto de partida para hacerse más fuerte. Por lo tanto, usted espera que su institución…",
        r1:"Le dé el aval para implementar su iniciativa con los compañeros con quienes comparte su interés.",
        r2:"Le indique el procedimiento institucional para lograr el aval de una propuesta estructurada y concreta.",
        r3:"Lo apoye con su iniciativa de forma que no se convierta en una carga laboral adicional y pueda construir con sus compañeros.",
        bueno:"r2",
        malo:"r3",
        regular:"r1"
    },
    {
        q:"3. En su entidad, se está fortaleciendo la cultura organizacional en torno al trabajo en equipo, por lo cual, usted espera que sus compañeros respondan de la siguiente manera…",
        r1:"Agradecerían ser considerados para la iniciativa más sin embargo desistirían de participar porque consideran que les representa trabajo adicional.",
        r2:"Acogerían su iniciativa y consideran que el interés que comparten potencialmente puede servirles para su crecimiento profesional y aportar a la organización.",
        r3:"Se comprometerían a participar de la iniciativa siempre y cuando se les presente una propuesta con objetivos definidos.",
        bueno:"r2",
        malo:"r1",
        regular:"r3"
    },
    {
        q:"4. Ha logrado convocar al equipo de trabajo conformado por aquellos compañeros de trabajo con quienes comparte un interés en común, en este primer encuentro usted…",
        r1:"Orienta una socialización mediante una lluvia de ideas sobre el interés compartido y su conceptualización para alinearlo con la misión organizacional.",
        r2:"Presenta el objetivo general de su idea, ya conceptualizada y alineada con la misión organizacional, para luego asignar responsabilidades a cada uno de los miembros del equipo.",
        r3:"Designa a uno de sus compañeros como líder para la construcción del proyecto y le solicita una propuesta para un siguiente encuentro con los respectivos objetivos general y específicos.",
        bueno:"r1",
        malo:"r3",
        regular:"r2"
    },
    {
        q:"5. Escrito el proyecto conjuntamente con su equipo de trabajo, el cual está alineando intereses compartidos con la misión organizacional, en un segundo o tercer encuentro usted…",
        r1:"Solicita a cada uno de sus compañeros que presente sus avances respecto a cada uno de los objetivos definidos en el proyecto.",
        r2:"Socializa una herramienta tecnológica que servirá para la construcción de actividades relacionadas con objetivos ya definidos.",
        r3:"Presenta un cronograma de actividades ya definidas y concretas en relación con cada uno de los objetivos establecidos.",
        bueno:"r2",
        malo:"r1",
        regular:"r3"
    },
    {
        q:"6. Para un tercer o cuarto encuentro de su equipo colaborativo de trabajo, usted…",
        r1:"Entrega las fechas de presentación de entregables y la correspondiente socialización de resultados a sus directivos.",
        r2:"Sugiere que como objetivo del encuentro se definan fechas y entregables para cada uno de los objetivos del proyecto colaborativo.",
        r3:"Revisa la producción conjunta de contenidos en la herramienta tecnológica dispuesta para ello y retroalimenta a sus compañeros.",
        bueno:"r2",
        malo:"r3",
        regular:"r1"
    },
    {
        q:"7. La ejecución del proyecto colaborativo se encuentra en curso, adelantada en un 45% de lo establecido en principio, para el siguiente encuentro del equipo usted…",
        r1:"Pone en discusión un resultado no esperado para que se considere como una oportunidad de mejora o redireccionamiento del proyecto.",
        r2:"Hace énfasis en los resultados logrados de acuerdo con objetivos propuestos y desestima los resultados negativos.",
        r3:"Realimenta los resultados no esperados y precisa las acciones que se deben implementar para ajustarlos a los objetivos.",
        bueno:"r1",
        malo:"r2",
        regular:"r3"
    },
    {
        q:"8. Usted identifica que el proyecto colaborativo tiene impactos positivos en los usuarios hacía los cuales están dirigidos algunos de sus objetivos específicos. Al respecto usted…",
        r1:"Presenta a su equipo un plan e instrumento de evaluación de impacto que les permita medirlo y socializarlo.",
        r2:"Menciona a su equipo que los impactos positivos son un valor agregado que podría estimarse para fortalecer los objetivos del proyecto.",
        r3:"Sugiere a su equipo hacer una consulta a los usuarios para identificar oportunidades de fortalecer el proyecto o crear otro.",
        bueno:"r3",
        malo:"r1",
        regular:"r2"
    },
    {
        q:"9. El proyecto colaborativo ha cumplido las metas principales propuestas, se socializa a las directivas los resultados. En la presentación usted…",
        r1:"Enfatiza en los principales resultados logrados para los objetivos principales del proyecto colaborativo producto de su idea inicial.",
        r2:"Orienta la presentación hacia los principales resultados, así como hacia reprocesos y procesos dados durante la ejecución.",
        r3:"Resalta oportunidades de reformulación de otro proyecto para articular con otras áreas y ofrece una herramienta para compartir.",
        bueno:"r3",
        malo:"r1",
        regular:"r2"
    },
    {
        q:"10. Su proyecto colaborativo ha sido producto de su iniciativa y el trabajo conjunto con un equipo de compañeros en su organización. Respecto a esta experiencia usted se siente…",
        r1:"Valorado por su organización y partícipe de un equipo de trabajo que ahora considera como compañeros de vida.",
        r2:"Exitoso, ha logrado aportar a su organización mediante un proyecto resultado de un trabajo en equipo.",
        r3:"Pleno, su trabajo hace parte de su desarrollo personal y profesional, aporta a su vida y a la de otros.",
        bueno:"r3",
        malo:"r2",
        regular:"r1"
    }
]
var paquete12=[
    {
        q:"1. Usted se siente molesto con la inconsistencia encontrada, por lo que considerando su personalidad y de acuerdo con la situación descrita, usted le diría a su colaborador…",
        r1:"“El informe tiene algunas inconsistencias importantes para revisar”",
        r2:"“Hay errores, pero no importa. Todos cometemos errores”",
        r3:"“Vuelva a revisarlo y me lo trae nuevamente después de mediodía”",
        bueno:"r1",
        malo:"r2",
        regular:"r3"
    },
    {
        q:"2. Usted quiere explicar nuevamente la forma y contenido del informe que requiere, para ello refiere lo siguiente…",
        r1:"(Con el informe en sus manos) “El documento está muy mal elaborado, revise algún modelo de informe para corregirlo”.",
        r2:"(Toma un papel, un lápiz y escribe) “Este es el orden de los contenidos del informe y debe revisar la relación entre información presentada e interpretación.",
        r3:"(Regresa el documento a su colaborados) “Por favor, entréguele a Fernando toda la información necesaria para que él haga el informe”.",
        bueno:"r2",
        malo:"r3",
        regular:"r1"
    },
    {
        q:"3. Usted mira a su colaborador y le dice…",
        r1:"“Bueno, cuénteme, ¿qué tanto conoce sobre la elaboración de este tipo de documentos?”",
        r2:"“Definitivamente esto implica un reproceso, hay que hacer este informe nuevamente”",
        r3:"“¿Quién le ayudó a hacer este trabajo? Parece que tampoco tiene idea de cómo se hace.",
        bueno:"r1",
        malo:"r3",
        regular:"r2"
    }
]
var paquete2=[
    {
        q:"1. Una vez usted ha decidido realizar el proyecto, En cuanto a actitud, usted…",
        r1:"Se comunica personalmente con sus compañeros de trabajo y los convoca a un encuentro informal para comentar su idea.",
        r2:"Revisa los documentos presentados por sus compañeros en la reunión y destaca aspectos en común con su trabajo.",
        r3:"Redacta un documento con una propuesta estructurada que cuenta con objetivos definidos y contempla acciones posibles.",
        bueno:"r1",
        malo:"r3",
        regular:"r2"
    },
    {
        q:"2. En su organización se está implementando el trabajo colaborativo como metodología para mejorar resultados. Por lo tanto, respecto a su iniciativa, usted espera que su institución…",
        r1:"Le reitere los procesos internos para la toma de decisiones en cuanto a iniciativas no establecidas en la planeación organizacional.",
        r2:"Lo anime a reunirse con sus compañeros de área para revisar a posteriori, cuál es la propuesta en concreto.",
        r3:"Le dé la oportunidad de reunirse con sus compañeros dentro de las horas laborales.",
        bueno:"r3",
        malo:"r1",
        regular:"r2"
    },
    {
        q:"3. En coherencia con el fortalecimiento de la cultura organizacional orientada hacia el trabajo colaborativo, dentro de su organización usted espera que sus compañeros de área…",
        r1:"Acojan su iniciativa y coincidan con lo que usted había notado en la reunión de cierre.",
        r2:"Invaliden su iniciativa y reiteren que sus procesos tendrán mejores resultados si siguen funcionando bajo la dinámica precedente.",
        r3:"Soliciten un documento con una propuesta estructurada que recoja los procesos que han adelantado individualmente.",
        bueno:"r1",
        malo:"r3",
        regular:"r2"
    },
    {
        q:"4. Para dar inicio al proceso, usted concreta un encuentro inicial con sus compañeros de área con el fin de revisar cómo definir un trabajo conjunto. Por lo cual, en este encuentro usted…",
        r1:"Designa a uno de sus compañeros como secretario para que redacte y socialice las actas de este encuentro y de los próximos.",
        r2:"Expone un objetivo preciso para el proyecto conjunto y solicita a sus compañeros retroalimentación.",
        r3:"Facilita el intercambio de ideas para construir un objetivo general mediante un software usado dentro de la organización.",
        bueno:"r3",
        malo:"r1",
        regular:"r2"
    },
    {
        q:"5. El proyecto se encuentra redactado y es producto de un intercambio de ideas. Por lo que, para un siguiente encuentro usted…",
        r1:"Presenta a sus compañeros un método para hacer seguimiento a proyectos con una actualización conjunta.",
        r2:"Asigna a cada uno de sus compañeros un determinado objetivo y la construcción de las actividades correspondientes.",
        r3:"Expone un cronograma con actividades concretas relacionadas con los objetivos propuestos.",
        bueno:"r1",
        malo:"r2",
        regular:"r3"
    },
    {
        q:"6. Para un tercer o cuarto encuentro de su equipo colaborativo de trabajo, usted…",
        r1:"Invita a sus compañeros a precisar actividades y posibles entregables con fechas correspondientes.",
        r2:"Socializa un cronograma de actividades con los entregables correspondientes y fechas de socialización a directivos.",
        r3:"Señala los desaciertos de sus compañeros respecto a la calidad de la información que han registrado en la producción conjunta.",
        bueno:"r1",
        malo:"r3",
        regular:"r2"
    },
    {
        q:"7. El proyecto colaborativo ha avanzado en un 40% de lo propuesto al inicio, para el siguiente encuentro con su equipo usted…",
        r1:"Señala los resultados no esperados con una serie de alternativas para ajustar los objetivos iniciales y así recuperar esa información.",
        r2:"Destaca los resultados negativos de acuerdo a los objetivos propuestos e indica que deben desestimarse dado que fueron errores de ejecución.",
        r3:"Propicia una reflexión colectiva sobre los resultados no esperados para que se propongan alternativas de orientación del proyecto.",
        bueno:"r3",
        malo:"r2",
        regular:"r1"
    },
    {
        q:"8. La implementación del proyecto colaborativo ha tenido un impacto positivo en el área de trabajo. Al respecto usted…",
        r1:"Propone a sus compañeros de equipo hacer una consulta a los demás colaboradores sobre su percepción del valor del proyecto.",
        r2:"Construye un instrumento estructurado de medición de impacto e indica a sus compañeros cómo y cuándo aplicarlo.",
        r3:"Discute con sus compañeros de equipo sobre mantener delimitado el proyecto y considerar efectos inesperados como valor agregado.",
        bueno:"r1",
        malo:"r2",
        regular:"r3"
    },
    {
        q:"9. El proyecto colaborativo del área ha sido implementado en un 100% de acuerdo con su objetivo inicial. En la presentación de resultados al área y la dirección, usted…",
        r1:"Valora sus resultados como producto de un esfuerzo conjunto cuya dinámica permitió tanto responder al objetivo inicial como identificar otras iniciativas.",
        r2:"Enfatiza en los resultados, entregables y evidencias correspondientes como resultado de un trabajo organizado y pertinente para el área.",
        r3:"Destaca los procesos desarrollados para el cumplimiento de los objetivos propuestos para el proyecto al inicio del proceso, mencionando las dificultades que se presentaron.",
        bueno:"r1",
        malo:"r2",
        regular:"r3"
    },
    {
        q:"10. Su proyecto colaborativo ha sido producto de un trabajo conjunto, realizado con un equipo de compañeros de área de su organización. Respecto a esta experiencia usted se siente…",
        r1:"Como una parte importante del área que ha cumplido con las funciones que le corresponden.",
        r2:"Identificado con actividades de su área que le permiten enriquecerse profesionalmente.",
        r3:"Conectado profundamente con su experiencia laboral, como espacio de crecimiento para su vida y su ser.",
        bueno:"r3",
        malo:"r1",
        regular:"r2"
    }
]
var paquete21=[
    {
        q:"1. En cuanto al llamado de atención usted…",
        r1:"Se mantiene en silencio y con ello acepta el llamado de atención recibido, el cual representa un memorando con copia a la hoja de vida de todos.",
        r2:"Refiere enfáticamente que se realice también un llamado de atención al área encargada del sistema, considerando que se les ha notificado sobre la falla.",
        r3:"Menciona que se tiene conocimiento de una falla en el sistema y que se ha notificado oportunamente, sugiere que se haga seguimiento al proceso.",
        bueno:"r3",
        malo:"r1",
        regular:"r2"
    },
    {
        q:"2. Usted pretende exponer el procedimiento seguido, respecto al motivo de las reclamaciones, para ello…",
        r1:"De forma pausada y firme dice: “Nosotros identificamos que el sistema estaba presentando fallas y realizamos la respectiva notificación”",
        r2:"De manera exasperada, levanta la voz y dice: “Se tiene que hacer un cambio en el área de sistemas y revisar la idoneidad de los encargados”",
        r3:"Mirando a sus compañeros de equipo dice: “Pues yo creo que esto es algo que nos compete a todos así que los escucho”.",
        bueno:"r1",
        malo:"r2",
        regular:"r3"
    },
    {
        q:"3. Usted sabe el impacto negativo que tiene para la marca de la organización este tipo de reclamaciones, de manera que usted interviene diciendo:",
        r1:"“Esta situación nos está perjudicando de manera importante, podríamos ofrecer algún beneficio adicional a los afectados y publicar una disculpa general en nuestra página”",
        r2:"“No hay que sentirnos culpables por lo que ha pasado, finalmente se identificó lo que estaba sucediendo y se siguió el procedimiento correspondiente”",
        r3:"“Yo me di cuenta de la falla del sistema hace más de un mes y le había comentado al ingeniero, pero parece que no me tomó en serio, entonces…”",
        bueno:"r1",
        malo:"r3",
        regular:"r2"
    }
]
function main(sym) {
var items=[];
var udec = ivo.structure({
        created:function(){
           var t=this;
            //precarga audios//
           ivo.load_audio(audios,onComplete=function(){
               t.animation();
               t.events();
               objetivo.play();
               t.text();
               ivo(ST+"preload").hide();
           });
        },
        methods: {
            scorm:function(nota){
                if(nota<=7){
                    return 10;
                }
                if(nota>=8 && nota <=14){
                    return 20;
                }
                if(nota>=18){
                    return 35;
                }
                if(nota>=19 && nota <=25){
                    return 4;
                }
                if(nota>=26){
                    return 50;
                }
            },
            retoalimetacion:function(id,puntaje){
                var t =this;
                if(id==="#retro11"){
                    if(puntaje >= 26){//retro bien
                        ivo(id).html(`<p><b>¡Felicitaciones!</b> Usted sabe cómo actuar para lograr un trabajo colaborativo dentro de su organización. El trabajo colaborativo surge, en su mayoría, de intereses compartidos alrededor de temas relacionados con la actividad laboral y se alinean con la misión de la institución. Requiere una cultura organizacional que lo promueva
                        mediante la disposición de recursos para adelantar iniciativas y permitir espacios de intercambio entre colaboradores dado que la organización los reconoce como recursos potenciales para mejorar su competitividad. Tanto la conceptualización como la definición de objetivos y actividades en un trabajo colaborativo se realiza de manera conjunta con el equipo. Se aceptan las relaciones horizontales y se implementan herramientas tecnológicas para facilitar el intercambio, producción y retroalimentación colectiva.</p>
                        <p>Durante la implementación de un proyecto colaborativo se debe prestar atención a la dinámica del proceso y considerar los resultados no esperados como información que permite perfeccionar el trabajo adelantado. De otro lado, su participación como parte de un trabajo colaborativo es importante, pues las iniciativas y sugerencias que usted aporta ayudan a concretar acciones y metas conducentes a productos o servicios concretos. Al participar activamente en un trabajo colaborativo, se actúa siempre de forma constructiva y propositiva, de acuerdo con los talentos de los colaboradores.
                        Los resultados de los proyectos colaborativos se presentan como resultante del trabajo y pensamiento colectivo. La experiencia en un trabajo colaborativo se convierte en fuente de satisfacción tanto personal como profesional. ¡Esta es una experiencia vital!</p>`);
                    }
                    if(puntaje >= 19 && puntaje <= 25){//retro regular
                        ivo(id).html(`<p><b>¡Hay camino por recorrer!</b>Pues según sus respuestas, usted propone acciones que pueden aportar al trabajo colaborativo, sin embargo, éstas presentan aspectos estructurados de un proyecto compartido que corresponde más al diseño de un trabajo cooperativo, el cual desconoce de cierta manera la producción colectiva. El trabajo colaborativo surge, en su mayoría, de intereses compartidos alrededor de temas relacionados con la actividad
                        laboral y se alinean con la misión de la institución. Requiere una cultura organizacional que lo promueva mediante la disposición de recursos para adelantar iniciativas y permitir espacios de intercambio entre colaboradores dado que la organización los reconoce como recursos potenciales para mejorar su competitividad. Tanto la conceptualización como la definición de objetivos y actividades en un trabajo colaborativo se realiza de manera conjunta con el equipo. Se aceptan las relaciones horizontales y se implementan herramientas tecnológicas para facilitar el intercambio, producción y retroalimentación colectiva.</p>
                        <p>Durante la implementación de un proyecto colaborativo se debe prestar atención a la dinámica del proceso y considerar los resultados no esperados como información que permite perfeccionar el trabajo adelantado. De otro lado, su participación como parte de un trabajo colaborativo es importante, pues las iniciativas y sugerencias que usted aporta ayudan a concretar acciones y metas conducentes a productos o servicios concretos. Al participar activamente en un trabajo colaborativo, se actúa siempre de forma constructiva y propositiva, de acuerdo con los talentos de los colaboradores.</p>
                        <p>Los resultados de los proyectos colaborativos se presentan como resultante del trabajo y pensamiento colectivo. La experiencia en un trabajo colaborativo se convierte en fuente de satisfacción tanto personal como profesional. <b>¡Esta es una experiencia vital!</b></p>`);
                    }
                    if(puntaje <= 18){//retro mal
                        ivo(id).html(`<p><b>¡Aún hay conocimientos por construir!</b> El trabajo colaborativo implica reconocerse como parte de una idea y de un colectivo, aprender a ser productivo en relación con otros. El trabajo colaborativo surge, en su mayoría, de intereses compartidos
                        alrededor de temas relacionados con la actividad laboral y se alinean con la misión de la institución. Requiere una cultura organizacional que lo promueva mediante la disposición de recursos para adelantar iniciativas y permitir espacios de intercambio entre colaboradores dado que la organización los reconoce como recursos potenciales para mejorar su competitividad. Tanto la conceptualización como la definición de objetivos y actividades en un trabajo colaborativo se realiza de manera conjunta con el equipo. Se aceptan las relaciones horizontales y se implementan herramientas tecnológicas para facilitar el intercambio, producción y retroalimentación colectiva.</p>
                        <p>Durante la implementación de un proyecto colaborativo se debe prestar atención a la dinámica del proceso y considerar los resultados no esperados como información que permite perfeccionar el trabajo adelantado. De otro lado, su participación como parte de un trabajo colaborativo es importante, pues las iniciativas y sugerencias que usted aporta ayudan a concretar acciones y metas conducentes a productos o servicios concretos. Al participar activamente en un trabajo colaborativo, se actúa siempre de forma constructiva y propositiva, de acuerdo con los talentos de los colaboradores.</p>
                        <p>Los resultados de los proyectos colaborativos se presentan como resultante del trabajo y pensamiento colectivo. La experiencia en un trabajo colaborativo se convierte en fuente de satisfacción tanto personal como profesional. ¡Esta es una experiencia vital!</p>`);
                    }
                    
                }
                if(id==="#retro1"){
                    if(puntaje >= 9){//retro bien
                        ivo(id).html(`<b>¡Excelente inicio! De acuerdo con las situaciones descritas usted respondió de manera que favorece la construcción de relaciones de confianza.</b> Recuerde que la confianza se hace evidente en las interacciones humanas. Las relaciones de confianza en la organización se construyen en cada una de las interacciones que se tienen con los colaboradores y se puede identificar en el grado de autenticidad que se observa en el otro, su capacidad para comunicar la lógica de lo que solicita o requiere, así como en la habilidad para ser empático. <b>Seguramente usted ha experimentado la confianza en las relaciones que ha establecido con colaboradores dentro de una organización y desde esta experiencia, identifica otros aspectos adicionales que la cualifican.</b>`);
                    }
                    if(puntaje >= 6 && puntaje <= 8){//retro regular
                        ivo(id).html(`<b>¡A Mejorar la interacción! Cada forma de interacción que sucede en las diferentes situaciones del contexto laboral son oportunidades para construir relaciones de confianza y la autobservación permite identificar la calidad de los vínculos que se construyen.</b> Recuerde que la confianza se hace evidente en las interacciones humanas. Las relaciones de confianza en la organización se construyen en cada una de las interacciones que se tienen con los colaboradores y se puede identificar en el grado de autenticidad que se observa en el otro, su capacidad para comunicar la lógica de lo que solicita o requiere, así como en la habilidad para ser empático. <b>Seguramente usted ha experimentado la confianza en las relaciones que ha establecido con colaboradores dentro de una organización y desde esta experiencia, identifica otros aspectos adicionales que la cualifican.</b>`);
                    }
                    if(puntaje <= 5){//retro mal
                        ivo(id).html(`<b>¡Fortalezca el autonocimiento y las experiencias de confianza! La disposición para generar confianza está relacionada con el nivel de conocimiento de sí mismo y las experiencias de socialización que han orientado la forma en que usted confía en otros.</b>  Recuerde que la confianza se hace evidente en las interacciones humanas. Las relaciones de confianza en la organización se construyen en cada una de las interacciones que se tienen con los colaboradores y se puede identificar en el grado de autenticidad que se observa en el otro, su capacidad para comunicar la lógica de lo que solicita o requiere, así como en la habilidad para ser empático. <b>Seguramente usted ha experimentado la confianza en las relaciones que ha establecido con colaboradores dentro de una organización y desde esta experiencia, identifica otros aspectos adicionales que la cualifican.</b>`);
                    }
                    //console.log(vistos1.length);
                    //console.log(puntaje);
                }
                if(id==="#retro22"){
                    if(puntaje >= 26){//retro bien
                        ivo(id).html(`<p><b>¡Felicitaciones!</b> Usted sabe cómo actuar para lograr un trabajo colaborativo dentro de su organización. El trabajo colaborativo surge, en su mayoría, de intereses compartidos alrededor de temas relacionados con la actividad laboral y se alinean con la misión de la institución. Requiere una cultura organizacional que lo promueva
                        mediante la disposición de recursos para adelantar iniciativas y permitir espacios de intercambio entre colaboradores dado que la organización los reconoce como recursos potenciales para mejorar su competitividad. Tanto la conceptualización como la definición de objetivos y actividades en un trabajo colaborativo se realiza de manera conjunta con el equipo. Se aceptan las relaciones horizontales y se implementan herramientas tecnológicas para facilitar el intercambio, producción y retroalimentación colectiva.</p>
                        <p>Durante la implementación de un proyecto colaborativo se debe prestar atención a la dinámica del proceso y considerar los resultados no esperados como información que permite perfeccionar el trabajo adelantado. De otro lado, su participación como parte de un trabajo colaborativo es importante, pues las iniciativas y sugerencias que usted aporta ayudan a concretar acciones y metas conducentes a productos o servicios concretos. Al participar activamente en un trabajo colaborativo, se actúa siempre de forma constructiva y propositiva, de acuerdo con los talentos de los colaboradores.
                        Los resultados de los proyectos colaborativos se presentan como resultante del trabajo y pensamiento colectivo. La experiencia en un trabajo colaborativo se convierte en fuente de satisfacción tanto personal como profesional. ¡Esta es una experiencia vital!</p>`);
                    }
                    if(puntaje >= 19 && puntaje <= 25){//retro regular
                        ivo(id).html(`<p><b>¡Hay camino por recorrer!</b>Pues según sus respuestas, usted propone acciones que pueden aportar al trabajo colaborativo, sin embargo, éstas presentan aspectos estructurados de un proyecto compartido que corresponde más al diseño de un trabajo cooperativo, el cual desconoce de cierta manera la producción colectiva. El trabajo colaborativo surge, en su mayoría, de intereses compartidos alrededor de temas relacionados con la actividad
                        laboral y se alinean con la misión de la institución. Requiere una cultura organizacional que lo promueva mediante la disposición de recursos para adelantar iniciativas y permitir espacios de intercambio entre colaboradores dado que la organización los reconoce como recursos potenciales para mejorar su competitividad. Tanto la conceptualización como la definición de objetivos y actividades en un trabajo colaborativo se realiza de manera conjunta con el equipo. Se aceptan las relaciones horizontales y se implementan herramientas tecnológicas para facilitar el intercambio, producción y retroalimentación colectiva.</p>
                        <p>Durante la implementación de un proyecto colaborativo se debe prestar atención a la dinámica del proceso y considerar los resultados no esperados como información que permite perfeccionar el trabajo adelantado. De otro lado, su participación como parte de un trabajo colaborativo es importante, pues las iniciativas y sugerencias que usted aporta ayudan a concretar acciones y metas conducentes a productos o servicios concretos. Al participar activamente en un trabajo colaborativo, se actúa siempre de forma constructiva y propositiva, de acuerdo con los talentos de los colaboradores.</p>
                        <p>Los resultados de los proyectos colaborativos se presentan como resultante del trabajo y pensamiento colectivo. La experiencia en un trabajo colaborativo se convierte en fuente de satisfacción tanto personal como profesional. <b>¡Esta es una experiencia vital!</b></p>`);
                    }
                    if(puntaje <= 18){//retro mal
                        ivo(id).html(`<p><b>¡Aún hay conocimientos por construir!</b> El trabajo colaborativo implica reconocerse como parte de una idea y de un colectivo, aprender a ser productivo en relación con otros. El trabajo colaborativo surge, en su mayoría, de intereses compartidos
                        alrededor de temas relacionados con la actividad laboral y se alinean con la misión de la institución. Requiere una cultura organizacional que lo promueva mediante la disposición de recursos para adelantar iniciativas y permitir espacios de intercambio entre colaboradores dado que la organización los reconoce como recursos potenciales para mejorar su competitividad. Tanto la conceptualización como la definición de objetivos y actividades en un trabajo colaborativo se realiza de manera conjunta con el equipo. Se aceptan las relaciones horizontales y se implementan herramientas tecnológicas para facilitar el intercambio, producción y retroalimentación colectiva.</p>
                        <p>Durante la implementación de un proyecto colaborativo se debe prestar atención a la dinámica del proceso y considerar los resultados no esperados como información que permite perfeccionar el trabajo adelantado. De otro lado, su participación como parte de un trabajo colaborativo es importante, pues las iniciativas y sugerencias que usted aporta ayudan a concretar acciones y metas conducentes a productos o servicios concretos. Al participar activamente en un trabajo colaborativo, se actúa siempre de forma constructiva y propositiva, de acuerdo con los talentos de los colaboradores.</p>
                        <p>Los resultados de los proyectos colaborativos se presentan como resultante del trabajo y pensamiento colectivo. La experiencia en un trabajo colaborativo se convierte en fuente de satisfacción tanto personal como profesional. ¡Esta es una experiencia vital!</p>`);
                    }
                }
                if(id==="#retro2"){
                    if(puntaje >= 9){//retro bien
                        ivo(id).html(`<b>¡Excelente inicio! De acuerdo con las situaciones descritas usted respondió de manera que favorece la construcción de relaciones de confianza.</b> Recuerde que la confianza se hace evidente en las interacciones humanas. Las relaciones de confianza en la organización se construyen en cada una de las interacciones que se tienen con los colaboradores y se puede identificar en el grado de autenticidad que se observa en el otro, su capacidad para comunicar la lógica de lo que solicita o requiere, así como en la habilidad para ser empático. <b>Seguramente usted ha experimentado la confianza en las relaciones que ha establecido con colaboradores dentro de una organización y desde esta experiencia, identifica otros aspectos adicionales que la cualifican.</b>`);
                    }
                    if(puntaje >= 6 && puntaje <= 8){//retro regular
                        ivo(id).html(`<b>¡A Mejorar la interacción! Cada forma de interacción que sucede en las diferentes situaciones del contexto laboral son oportunidades para construir relaciones de confianza y la autobservación permite identificar la calidad de los vínculos que se construyen.</b> Recuerde que la confianza se hace evidente en las interacciones humanas. Las relaciones de confianza en la organización se construyen en cada una de las interacciones que se tienen con los colaboradores y se puede identificar en el grado de autenticidad que se observa en el otro, su capacidad para comunicar la lógica de lo que solicita o requiere, así como en la habilidad para ser empático. <b>Seguramente usted ha experimentado la confianza en las relaciones que ha establecido con colaboradores dentro de una organización y desde esta experiencia, identifica otros aspectos adicionales que la cualifican.</b>`);
                    }
                    if(puntaje <= 5){//retro mal
                        ivo(id).html(`<b>¡Fortalezca el autonocimiento y las experiencias de confianza! La disposición para generar confianza está relacionada con el nivel de conocimiento de sí mismo y las experiencias de socialización que han orientado la forma en que confía en otros.</b>  Recuerde que la confianza se hace evidente en las interacciones humanas. Las relaciones de confianza en la organización se construyen en cada una de las interacciones que se tienen con los colaboradores y se puede identificar en el grado de autenticidad que se observa en el otro, su capacidad para comunicar la lógica de lo que solicita o requiere, así como en la habilidad para ser empático. <b>Seguramente usted ha experimentado la confianza en las relaciones que ha establecido con colaboradores dentro de una organización y desde esta experiencia, identifica otros aspectos adicionales que la cualifican.</b>`);
                    }
                    if(vistos2.length==13){
                        //enviar centro califiaciones
                        Scorm_mx = new MX_SCORM(false);
                        console.log("Nombre: " + Scorm_mx.info_user().name + " Id: " + Scorm_mx.info_user().id);
                        Scorm_mx.set_score(t.scorm(puntaje));
                    }
                }
                ivo(id).css("font-size","11px");
                console.log(puntaje);
                console.log(id);
            },
            text:function(){
                var t=this;
                //Slider 1
                ivo(ST+"main1").append(`<div  class="pages1" > <h1>Situación 1</h1> Usted se ha dado cuenta de que comparte intereses con otros compañeros de trabajo, tanto de su área como de otras áreas de la organización. Encuentra que ese interés en común puede ser una oportunidad para crear un proyecto en conjunto.</div>`);
                let n=1;
                for(p1 of paquete1){
                    ivo(ST+"main1").append(`<div class="pages1" ><div  class="q">${p1.q}</div><ul class="link1" id="n-${n}" data-bien="${p1.bueno}" data-regular="${p1.regular}" data-mal="${p1.malo}" ><li data-id="n-${n}" id="r1" >${p1.r1}</li><li data-id="n-${n}" id="r2">${p1.r2}</li><li data-id="n-${n}" id="r3">${p1.r3}</li></ul></div>`);
                    n+=1;
                }
                ivo(ST+"main1").append(`<div id="retro11" class="pages1" ></div>`);
                ivo(ST+"main1").append(`<div  class="pages1" > <h1>Situación 1</h1> <b>Usted ocupa un cargo directivo en la organización. Se caracteriza por ser una persona autocrítica, directa, organizada y creativa.</b> Se encuentra en su oficina cuando uno de sus colaboradores de área se acerca para solicitar la realimentación al informe que usted solicitó. En presencia de su colaborador usted hace la revisión del informe y encuentra información inconsistente entre la información gráfica y la interpretación de esta, así como incoherencias en la organización de los contenidos de acuerdo con su solicitud inicial. </div>`);
                for(p1 of paquete12){
                    ivo(ST+"main1").append(`<div class="pages1" ><div  class="q">${p1.q}</div><ul class="link11" id="n-${n}" data-bien="${p1.bueno}" data-regular="${p1.regular}" data-mal="${p1.malo}" ><li data-id="n-${n}" id="r1" >${p1.r1}</li><li data-id="n-${n}" id="r2">${p1.r2}</li><li data-id="n-${n}" id="r3">${p1.r3}</li></ul></div>`);
                    n+=1;
                }
                ivo(ST+"main1").append(`<div id="retro1" class="pages1" ></div>`);

                slider1= ivo(ST+"main1").slider({
                    slides:'.pages1',
                    progress:"n1",
                    btn_next:ST+"btn_sig",
                    btn_back:ST+"btn_atras",
                    onMove:function(page){
                        ivo.play("click");
                        setTimeout(function(){
                            if(page==1){
                                caso1.play();
                            }
                            if(page==13){
                                caso12.play();
                            }
                            if(page==17){
                                let puntaje=puntos1+puntos11;
                                console.log (puntaje);
                                if(puntaje<=25){
                                    swal({
                                        title: "El resultado no fue el esperado!!",
                                        text: "¿Desea intentarlo nuevamente con otra situación problémica?, recuerde que la última nota se verá reflejada en el centro calificaciones.",
                                        icon: "warning",
                                        buttons: true,
                                        dangerMode: true,
                                        buttons: ["No", "Si"],
                                      })
                                      .then((willDelete) => {
                                        if (willDelete) {
                                          //si
                                          test1.reverse().timeScale(3);
                                          test2.play();
                                          caso2.play();
                                          ivo(ST+"situacion2").hide();
                                        } else {
                                          //enviar centro califiaciones
                                          Scorm_mx = new MX_SCORM(false);
                                          console.log("Nombre: " + Scorm_mx.info_user().name + " Id: " + Scorm_mx.info_user().id);
                                          Scorm_mx.set_score(t.scorm(puntaje));
                                          swal({
                                            title: "Resultado!!",
                                            text: "Su puntaje fue de "+puntaje+", su nota es: "+t.scorm(puntaje),
                                            icon: "warning",
                                            buttons: true,
                                         })
                                        }
                                    });
                                }else{
                                    Scorm_mx = new MX_SCORM(false);
                                    console.log("Nombre: " + Scorm_mx.info_user().name + " Id: " + Scorm_mx.info_user().id);
                                    Scorm_mx.set_score(t.scorm(puntaje));
                                    swal({
                                        title: "Resultado!!",
                                        text: "Su puntaje fue de "+puntaje+", su nota es: "+t.scorm(puntaje),
                                        icon: "warning",
                                        buttons: true,
                                     })
                                }
                                
                            }
                        },1000);
                    },
                    onFinish:function(){
                    }
                });

                //Slider 2
                ivo(ST+"main2").append(`<div  class="pages2" > <h1>Situación 2</h1> En una reunión de cierre de área usted descubre que tres de sus compañeros adelantaron procesos similares a los que usted adelantó, lo cual encuentra motivante para construir un proyecto conjunto alineado con los objetivos del área organizacional a la que pertenece. A continuación, encontrará una serie de planteamientos que le orientarán los pasos a seguir para proponer un trabajo colaborativo de principio a fin. A partir de los planteamientos en mención, usted avanzará en el proceso de desarrollo del proyecto antes mencionado, seleccionando cuidadosamente las diferentes opciones que dan respuesta contundente a cada uno.</div>`);
                let n2=1;
                for(p1 of paquete2){
                    ivo(ST+"main2").append(`<div class="pages2" ><div  class="q">${p1.q}</div><ul class="link2" id="n-${n}" data-bien="${p1.bueno}" data-regular="${p1.regular}" data-mal="${p1.malo}" ><li data-id="n-${n}" id="r1" >${p1.r1}</li><li data-id="n-${n}" id="r2">${p1.r2}</li><li data-id="n-${n}" id="r3">${p1.r3}</li></ul></div>`);
                    n+=1;
                }
                ivo(ST+"main2").append(`<div id="retro22" class="pages2" ></div>`);
                ivo(ST+"main2").append(`<div  class="pages2" > <h1>Situación 2</h1> <b>Usted hace parte de un equipo de trabajo con una trayectoria de dos años en la misma área. se caracteriza por ser una persona tranquila, ecuánime, respetuosa y proactiva.</b> Su equipo de trabajo es citado a la oficina de dirección dado que han recibido frecuentes reclamaciones durante el último mes respecto a la entrega oportuna de productos adquiridos on line. Usted sabe que el sistema ha estado fallando y el equipo lo ha notificado recientemente tanto a la dirección como al área encargada sin recibir respuesta alguna.</div>`);
                for(p1 of paquete21){
                    ivo(ST+"main2").append(`<div class="pages2" ><div  class="q">${p1.q}</div><ul class="link22" id="n-${n2}" data-bien="${p1.bueno}" data-regular="${p1.regular}" data-mal="${p1.malo}" ><li data-id="n-${n2}" id="r1" >${p1.r1}</li><li data-id="n-${n2}" id="r2">${p1.r2}</li><li data-id="n-${n2}" id="r3">${p1.r3}</li></ul></div>`);
                    n2+=1;
                }
                ivo(ST+"main2").append(`<div id="retro2" class="pages2" ></div>`);
                slider2= ivo(ST+"main2").slider({
                    slides:'.pages2',
                    progress:"n2",
                    btn_next:ST+"btn_sig2",
                    btn_back:ST+"btn_atras2",
                    onMove:function(page){
                        ivo.play("click");
                        setTimeout(function(){
                            if(page==1){
                                caso2.play();
                            }
                            if(page==13){
                                caso22.play();
                            }
                            if(page==17){
                                let puntaje=0;
                                puntaje=puntos2+puntos22;
                                //enviar centro califiaciones
                                Scorm_mx = new MX_SCORM(false);
                                console.log("Nombre: " + Scorm_mx.info_user().name + " Id: " + Scorm_mx.info_user().id);
                                Scorm_mx.set_score(t.scorm(puntaje));
                            }
                        },1000);
                    },
                    onFinish:function(){
                    }
                });
                    ivo(".link1 li").on("click",function(){
                        let id = ivo(this).attr("data-id");
                        let dni = ivo(this).attr("id");
                        let access=true;
                        for (v of vistos1){
                            if(v==id){
                                access=false;
                            }
                        }
                        if(access){
                            if(ivo("#"+id).attr("data-bien")==dni){
                                ivo(this).addClass("bueno");
                                puntos1 += 3;
                                good.play();
                            }
                            if(ivo("#"+id).attr("data-regular")==dni){
                                ivo(this).addClass("regular");
                                puntos1 += 2;
                                regular.play();
                            }
                            if(ivo("#"+id).attr("data-mal")==dni){
                                ivo(this).addClass("mal");
                                puntos1 += 1;
                                fail.play();
                            }
                            vistos1.push(id);
                        }
                        t.retoalimetacion("#retro11",puntos1);
                    });

                    ivo(".link11 li").on("click",function(){
                        let id = ivo(this).attr("data-id");
                        let dni = ivo(this).attr("id");
                        let access=true;
                        for (v of vistos11){
                            if(v==id){
                                access=false;
                            }
                        }
                        if(access){
                            if(ivo("#"+id).attr("data-bien")==dni){
                                ivo(this).addClass("bueno");
                                puntos11 += 3;
                                good.play();
                            }
                            if(ivo("#"+id).attr("data-regular")==dni){
                                ivo(this).addClass("regular");
                                regular.play();
                                puntos11 += 2;
                            }
                            if(ivo("#"+id).attr("data-mal")==dni){
                                ivo(this).addClass("mal");
                                puntos11 += 1;
                                fail.play();
                            }
                            vistos11.push(id);
                        }
                        t.retoalimetacion("#retro1",puntos11);
                    });
                    ivo(".link22 li").on("click",function(){
                        let id = ivo(this).attr("data-id");
                        let dni = ivo(this).attr("id");
                        let access=true;
                        for (v of vistos22){
                            if(v==id){
                                access=false;
                            }
                        }
                        if(access){
                            if(ivo("#"+id).attr("data-bien")==dni){
                                ivo(this).addClass("bueno");
                                puntos22 += 3;
                                good.play();
                            }
                            if(ivo("#"+id).attr("data-regular")==dni){
                                ivo(this).addClass("regular");
                                puntos22 += 2;
                                regular.play();
                            }
                            if(ivo("#"+id).attr("data-mal")==dni){
                                ivo(this).addClass("mal");
                                puntos22 += 1;
                                fail.play();
                            }
                            vistos22.push(id);
                        }
                        t.retoalimetacion("#retro2",puntos22);
                    });
                    ivo(".link2 li").on("click",function(){
                        let id = ivo(this).attr("data-id");
                        let dni = ivo(this).attr("id");
                        let access=true;
                        for (v of vistos2){
                            if(v==id){
                                access=false;
                            }
                        }
                        if(access){
                            if(ivo("#"+id).attr("data-bien")==dni){
                                ivo(this).addClass("bueno");
                                puntos2 += 3;
                                good.play();
                            }
                            if(ivo("#"+id).attr("data-regular")==dni){
                                ivo(this).addClass("regular");
                                puntos2 += 2;
                                regular.play();
                            }
                            if(ivo("#"+id).attr("data-mal")==dni){
                                ivo(this).addClass("mal");
                                puntos2 += 1;
                                fail.play();
                            }
                            vistos2.push(id);
                        }
                        t.retoalimetacion("#retro22",puntos2);
                    });
            
            },
            events:function(){
                var t=this;
                ivo(ST+"situacion2").hide();
                ivo(ST+"btn_objetivos").on("click",function(){
                    objetivo.play().timeScale(1);
                    test1.reverse().timeScale(10);
                    test2.reverse().timeScale(10);
                    caso1.stop();
                    caso2.stop();
                });
                ivo(ST+"btn_instruccion").on("click",function(){
                    instruccion.play().timeScale(1);
                    test1.reverse().timeScale(10);
                    test2.reverse().timeScale(10);
                    caso1.stop();
                    caso2.stop();
                });
                ivo(ST+"btn_creditos").on("click",function(){
                    creditos.play().timeScale(1);
                    test1.reverse().timeScale(10);
                    test2.reverse().timeScale(10);
                    caso1.stop();
                    caso2.stop();
                });

                ivo(ST+"cerrar").on("click",function(){
                    objetivo.reverse().timeScale(3);
                });
                ivo(ST+"cerrar2").on("click",function(){
                    instruccion.reverse().timeScale(3);
                });
                ivo(ST+"frm_creditos").on("click",function(){
                    creditos.reverse().timeScale(3);
                });

                ivo(ST+"situacion1").on("click",function(){
                    test1.play().timeScale(1);
                    caso1.play();
                });

                ivo(ST+"situacion2").on("click",function(){
                    test2.play().timeScale(1);
                    caso2.play();
                });
            },
            animation:function(){
                creditos = new TimelineMax();
                creditos.append(TweenMax.from(ST+"frm_creditos", .8,  {x:-1200,opacity:0}), 0);
                creditos.append(TweenMax.from(ST+"Recurso_19", .8,    {x:-1200, opacity:0}), 0);
                creditos.stop();

                instruccion = new TimelineMax();
                instruccion.append(TweenMax.from(ST+"frm_instruccion", .8,  {x:-1200,opacity:0}), 0);
                instruccion.append(TweenMax.from(ST+"cerrar2", .8,    {y:200}), 0);
                instruccion.stop();

                objetivo = new TimelineMax();
                objetivo.append(TweenMax.from(ST+"frm_objetivo", .8,  {x:-1200,opacity:0}), 0);
                objetivo.append(TweenMax.from(ST+"cerrar", .8,    {y:200}), 0);
                objetivo.stop();

                test1 = new TimelineMax();
                test1.append(TweenMax.from(ST+"frm_test1", .8,  {x:-1200,opacity:0}), 0);
                test1.stop();

                test2 = new TimelineMax();
                test2.append(TweenMax.from(ST+"frm_test2", .8,  {x:-1200,opacity:0}), 0);
                test2.stop();
            }
        }
 });
}