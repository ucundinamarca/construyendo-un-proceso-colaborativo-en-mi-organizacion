## Construyendo un proceso colaborativo en mi organización
> ​Implementar los aspectos básicos asociados a la cultura del trabajo colaborativo respecto a una situación específica, direccionando a partir de ellos el curso adecuado de un trabajo colaborativo en cualquier organización.



![Texto alternativo](/images/Recurso_2.png "Construyendo un proceso colaborativo en mi organización")
> Metadatos del programador

~~~~js
ivo.info
~~~~

* * *
> Variables con los audios objeto virtual y algunas imagenes de los popup

~~~~js 
let popup1
~~~~


* * *
> Variable que activa dos conportamientos del siguiente de objetivos, con true da continuidad a la siguiente animación, con false solo retrocede la animación de objetivos

~~~~js 
var block=true;
~~~~

> Inicializador disparador de eventos y animaciones 



~~~~js 
var udec = ivo.structure({
    created:function(){
        var t=this;
        ivo.load_audio(popup1,function(){
            ivo(ST+"preload").hide();
            scena1.play();
        });
        //llama los eventos para asociar lo oyentes
        this.events(); 
        //lama las animaciones para ponerlas en pila
        this.animation(); 
    }
}
~~~~

> Eventos
~~~~js 
events:function(){
    
},
~~~~

> Animaciones

~~~~js 
animation:function(){

}
~~~~