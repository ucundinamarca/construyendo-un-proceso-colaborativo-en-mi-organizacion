/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='images/',
        aud='media/',
        vid='media/',
        js='js/',
        fonts = {
        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "both",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: 'fondo',
                            type: 'image',
                            rect: ['0px', '0px', '1024px', '640px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im+"Recurso_2.png",'0px','0px']
                        },
                        {
                            id: 'titulo',
                            type: 'image',
                            rect: ['28px', '15px', '966px', '71px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im+"Recurso_1.png",'0px','0px']
                        },
                        {
                            id: 'btn_creditos',
                            type: 'image',
                            rect: ['28px', '298px', '160px', '52px', 'auto', 'auto'],
                            cursor: 'pointer',
                            fill: ["rgba(0,0,0,0)",im+"Recurso_5.png",'0px','0px']
                        },
                        {
                            id: 'btn_instruccion',
                            type: 'image',
                            rect: ['28px', '234px', '160px', '51px', 'auto', 'auto'],
                            cursor: 'pointer',
                            fill: ["rgba(0,0,0,0)",im+"Recurso_4.png",'0px','0px']
                        },
                        {
                            id: 'btn_objetivos',
                            type: 'image',
                            rect: ['28px', '169px', '160px', '51px', 'auto', 'auto'],
                            cursor: 'pointer',
                            fill: ["rgba(0,0,0,0)",im+"Recurso_3.png",'0px','0px']
                        },
                        {
                            id: 'situacion2',
                            type: 'image',
                            rect: ['648px', '356px', '214px', '148px', 'auto', 'auto'],
                            cursor: 'pointer',
                            fill: ["rgba(0,0,0,0)",im+"Recurso_9.png",'0px','0px']
                        },
                        {
                            id: 'situacion1',
                            type: 'image',
                            rect: ['648px', '169px', '214px', '147px', 'auto', 'auto'],
                            cursor: 'pointer',
                            fill: ["rgba(0,0,0,0)",im+"Recurso_8.png",'0px','0px']
                        },
                        {
                            id: 'frm_test1',
                            type: 'group',
                            rect: ['483', '146', '496', '455', 'auto', 'auto'],
                            c: [
                            {
                                id: 'cuadro',
                                type: 'image',
                                rect: ['0px', '0px', '496px', '455px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"cuadro.png",'0px','0px']
                            },
                            {
                                id: 'group_main1',
                                type: 'group',
                                rect: ['20', '19', '460', '405', 'auto', 'auto'],
                                clip: 'rect(0px 460px 405px 0px)',
                                c: [
                                {
                                    id: 'main1',
                                    type: 'rect',
                                    rect: ['0px', '0px', '460px', '405px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,1.00)"],
                                    stroke: [0,"rgba(0,0,0,1)","none"]
                                }]
                            },
                            {
                                id: 'btn_atras',
                                type: 'image',
                                rect: ['19px', '382px', '51px', '61px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_17.png",'0px','0px']
                            },
                            {
                                id: 'btn_sig',
                                type: 'image',
                                rect: ['417px', '382px', '58px', '61px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_18.png",'0px','0px']
                            }]
                        },
                        {
                            id: 'frm_test2',
                            display: 'block',
                            type: 'group',
                            rect: ['483', '146', '496', '455', 'auto', 'auto'],
                            c: [
                            {
                                id: 'cuadro2',
                                type: 'image',
                                rect: ['0px', '0px', '496px', '455px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"cuadro.png",'0px','0px']
                            },
                            {
                                id: 'group_main2',
                                type: 'group',
                                rect: ['20', '19', '460', '405', 'auto', 'auto'],
                                clip: 'rect(0px 460px 405px 0px)',
                                c: [
                                {
                                    id: 'main2',
                                    type: 'rect',
                                    rect: ['0px', '0px', '460px', '405px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,1.00)"],
                                    stroke: [0,"rgba(0,0,0,1)","none"]
                                }]
                            },
                            {
                                id: 'btn_atras2',
                                type: 'image',
                                rect: ['19px', '382px', '51px', '61px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_17.png",'0px','0px']
                            },
                            {
                                id: 'btn_sig2',
                                type: 'image',
                                rect: ['417px', '382px', '58px', '61px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_18.png",'0px','0px']
                            }]
                        },
                        {
                            id: 'frm_objetivo',
                            type: 'group',
                            rect: ['10px', '155', '984px', '447', 'auto', 'auto'],
                            c: [
                            {
                                id: 'box-background',
                                type: 'image',
                                rect: ['541px', '-9px', '428px', '447px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_6.png",'0px','0px']
                            },
                            {
                                id: 'cerrar',
                                type: 'image',
                                rect: ['675px', '353px', '160px', '51px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"Recurso_7.png",'0px','0px']
                            },
                            {
                                id: 'c1',
                                type: 'text',
                                rect: ['569px', '86px', '352px', '200px', 'auto', 'auto'],
                                text: "<p style=\"margin: 0px; text-align: justify;\">​Implementar los aspectos básicos asociados a la cultura del trabajo colaborativo respecto a una situación específica, direccionando a partir de ellos el curso adecuado de un trabajo colaborativo en cualquier organización.</p>",
                                font: ['Arial, Helvetica, sans-serif', [24, ""], "rgba(0,0,0,1)", "normal", "none", "", "break-word", "normal"]
                            },
                            {
                                id: 't1',
                                type: 'text',
                                rect: ['567px', '14px', '375px', '42px', 'auto', 'auto'],
                                text: "<p style=\"margin: 0px; text-align: justify;\">Objetivo</p>",
                                font: ['Arial, Helvetica, sans-serif', [34, "px"], "rgba(0,0,0,1)", "normal", "none", "", "break-word", "normal"]
                            },
                            {
                                id: 'linea',
                                type: 'image',
                                rect: ['567px', '57px', '352px', '2px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"linea.svg",'0px','0px']
                            }]
                        },
                        {
                            id: 'frm_instruccion',
                            type: 'group',
                            rect: ['10px', '155', '984px', '447', 'auto', 'auto'],
                            c: [
                            {
                                id: 'box-backgroundCopy',
                                type: 'image',
                                rect: ['541px', '-9px', '428px', '447px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_6.png",'0px','0px']
                            },
                            {
                                id: 'cerrar2',
                                type: 'image',
                                rect: ['675px', '361px', '160px', '51px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"Recurso_7.png",'0px','0px']
                            },
                            {
                                id: 'c1Copy',
                                type: 'text',
                                rect: ['569px', '68px', '352px', '200px', 'auto', 'auto'],
                                text: "<p style=\"margin: 0px; text-align: justify;\">​A continuación, encontrará dos (2) situaciones, una para proponer un trabajo colaborativo de principio a fin y la otra para explorar la construcción de relaciones de confianza. Encontrará adicionalmente, algunos planteamientos en torno a ellas. Seleccione cuidadosamente, las opciones que dan respuesta contundente a los planteamientos en cuestión.</p><p style=\"margin: 0px;\">​</p><p style=\"margin: 0px;\">De acuerdo a cada respuesta seleccionada, usted podrá obtener tres (3) puntos, si ésta es la de mayor pertinencia, dos (2) puntos si su respuesta se aproxima a las prácticas colaborativas y un (1) punto si su respuesta no es acertada. El puntaje más alto que puede alcanzar es de treinta y nueve (39) puntos. Analice detenidamente sus respuestas teniendo en cuenta lo trabajado en el presente REA.<span style=\"font-weight: 700;\"> ¡Ánimo y adelante!</span></p><p style=\"margin: 0px;\">​</p><p style=\"margin: 0px;\"></p>",
                                font: ['Arial, Helvetica, sans-serif', [15, "px"], "rgba(0,0,0,1)", "normal", "none", "", "break-word", "normal"]
                            },
                            {
                                id: 't1Copy',
                                type: 'text',
                                rect: ['567px', '14px', '375px', '42px', 'auto', 'auto'],
                                text: "<p style=\"margin: 0px;\">​Instrucción</p>",
                                font: ['Arial, Helvetica, sans-serif', [34, "px"], "rgba(0,0,0,1)", "normal", "none", "", "break-word", "normal"]
                            },
                            {
                                id: 'lineaCopy',
                                type: 'image',
                                rect: ['567px', '57px', '352px', '2px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"linea.svg",'0px','0px']
                            }]
                        },
                        {
                            id: 'frm_creditos',
                            type: 'group',
                            rect: ['0', '0', '1024', '640', 'auto', 'auto'],
                            c: [
                            {
                                id: 'Rectangle',
                                type: 'rect',
                                rect: ['0px', '0px', '1024px', '640px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0.64)"],
                                stroke: [0,"rgba(0,0,0,1)","none"]
                            },
                            {
                                id: 'Recurso_19',
                                type: 'image',
                                rect: ['109px', '45px', '806px', '550px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_19.png",'0px','0px']
                            }]
                        },
                        {
                            id: 'preload',
                            type: 'group',
                            rect: ['0', '0px', '1024', '640', 'auto', 'auto'],
                            c: [
                            {
                                id: 'Rectangle3',
                                type: 'rect',
                                rect: ['0px', '0px', '1024px', '640px', 'auto', 'auto'],
                                fill: ["rgba(255,255,255,1.00)"],
                                stroke: [0,"rgba(0,0,0,1)","none"]
                            },
                            {
                                id: 'Free-Loading-GIF-Icons',
                                type: 'image',
                                rect: ['327px', '168px', '400px', '300px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Free-Loading-GIF-Icons.gif",'0px','0px']
                            }]
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: ['null', 'null', '1024px', '640px', 'auto', 'auto'],
                            overflow: 'hidden',
                            fill: ["rgba(255,255,255,1)"]
                        }
                    }
                },
                timeline: {
                    duration: 0,
                    autoPlay: true,
                    data: [
                        [
                            "eid1",
                            "display",
                            0,
                            0,
                            "linear",
                            "${frm_test2}",
                            'block',
                            'block'
                        ]
                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

    if (!window.edge_authoring_mode) AdobeEdge.getComposition(compId).load("index2_edgeActions.js");
})("EDGE-397919");
